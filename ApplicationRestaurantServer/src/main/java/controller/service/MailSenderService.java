package controller.service;

import controller.mail.Mail;
import controller.mail.SendGrid;
import controller.mail.SendGridException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * Created by Alin on 5/22/2016.
 */
@Service("mailSender")
public class MailSenderService {

    private final static String SENDGRID_API_KEY = "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";

    public ResponseEntity sendEmail(Mail mail){

        SendGrid sendgrid = new SendGrid(SENDGRID_API_KEY);
        SendGrid.Email email= new SendGrid.Email();
        email.addTo(mail.getTo());
        email.setFrom(mail.getFrom());
        email.setSubject(mail.getSubject());
        email.setText(mail.getMessage());

        try {
            sendgrid.send(email);
            return new ResponseEntity("Success", HttpStatus.OK);
        } catch (SendGridException e) {
            e.printStackTrace();
            return new ResponseEntity("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

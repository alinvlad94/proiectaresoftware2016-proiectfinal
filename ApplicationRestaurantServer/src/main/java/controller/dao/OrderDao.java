package controller.dao;

import controller.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Alin on 5/20/2016.
 */

@Component("orderDao")
public class OrderDao {

    private NamedParameterJdbcTemplate jdbc;

    public List<Order> getAllOrders(){
        return jdbc.query("select * from orders", new RowMapper<Order>() {
            @Override
            public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
                Order order = new Order();
                order.setIdOrder(rs.getInt("idOrder"));
                order.setIdProdus(rs.getInt("idProduct"));
                order.setIdVoucher(rs.getInt("idVoucher"));
                order.setPieces(rs.getInt("pieces"));
                order.setPrice(rs.getInt("price"));
                order.setName(rs.getString("name"));
                order.setState_activ(rs.getInt("state_activ"));
                order.setTable_number(rs.getInt("table_number"));
                order.setWaitress_name(rs.getString("waitress_name"));
                return order;
            }
        });
    }

    public void insertOrder(Order order){
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("idOrder", getAutoIncrement());
        params.addValue("idProdus",order.getIdProdus());
        params.addValue("idVoucher",order.getIdVoucher());
        params.addValue("pieces",order.getPieces());
        params.addValue("price", order.getPrice());
        params.addValue("state_activ", order.getState_activ());
        params.addValue("name",order.getName());
        params.addValue("table_number",order.getTable_number());
        params.addValue("waitress_name", "a");
        jdbc.update("insert into orders(idOrder,idProduct,idVoucher,pieces,price,state_activ,table_number,waitress_name,name) values (:idOrder,:idProdus,:idVoucher,:pieces,:price,:state_activ,:table_number,:waitress_name,:name)",params);

    }

    public void makeOrderUnactive(int table_number){
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("table_number",table_number);
        params.addValue("state_activ",0);
        params.addValue("activ",1);
        jdbc.update("update orders set state_activ =:state_activ where table_number=:table_number and state_activ=:activ",params);
    }

    public int getAutoIncrement(){
        List<Order> orders= getAllOrders();
        int max = 0;
        if(orders.size()>0){
            for(Order order: orders){
                if(order.getIdOrder() > max){
                    max = order.getIdOrder();
                }
            }
        }
        return max +1;
    }

    @Autowired
    @Qualifier("dataSource")
    public void setDataSource(DataSource jdbc) {
        this.jdbc = new NamedParameterJdbcTemplate(jdbc);
    }
}

package controller.dao;

import controller.model.Order;
import controller.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alin on 5/21/2016.
 */

@Component("productDao")
public class ProductDao {

    private NamedParameterJdbcTemplate jdbc;

    public List<Product> getAllProducts() {
        return jdbc.query("select * from product", new RowMapper<Product>() {
            @Override
            public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
                Product product = new Product();
                product.setIdProduct(rs.getInt("idProduct"));
                product.setDescription(rs.getString("description"));
                product.setName(rs.getString("nameProduct"));
                product.setPhoto(rs.getString("photo"));
                product.setPrice(rs.getInt("price"));
                product.setCategory(rs.getString("category"));
                return product;
            }
        });
    }

    public void insertProduct(Product product){
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("idProduct", getAutoIncrement());
        params.addValue("nameProduct",product.getName());
        params.addValue("category",product.getCategory());
        params.addValue("description",product.getDescription());
        params.addValue("price", product.getPrice());
        params.addValue("photo",product.getPhoto());
        jdbc.update("insert into product(idProduct,nameProduct,category,description,price,photo) " +
                "values (:idProduct,:nameProduct,:category,:description,:price,:photo)",params);
    }

    public void updateProduct(Product product){
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("idProduct",product.getIdProduct());
        params.addValue("price",product.getPrice());
        params.addValue("nameProduct",product.getName());
        params.addValue("description",product.getDescription());
        params.addValue("category",product.getCategory());
        params.addValue("photo",product.getPhoto());
        jdbc.update("update product set price= :price, nameProduct= :nameProduct, description= :description, category= :category, photo= :photo where idProduct=:idProduct",params);
    }

    public void deleteProduct(int idProduct){
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("idProduct", idProduct);

        jdbc.update("delete from product where idProduct=:idProduct", params);
    }

    public int getAutoIncrement(){
        List<Product> products= getAllProducts();
        int max = 0;
        if(products.size()>0){
            for(Product product: products){
                if(product.getIdProduct() > max){
                    max = product.getIdProduct();
                }
            }
        }
        return max +1;
    }

    @Autowired
    @Qualifier("dataSource")
    public void setDataSource(DataSource jdbc) {
        this.jdbc = new NamedParameterJdbcTemplate(jdbc);
    }

}

package controller.dao;

import controller.model.Order;
import controller.model.Voucher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Alin on 5/22/2016.
 */

@Component("voucherDao")
public class VoucherDao {

    private NamedParameterJdbcTemplate jdbc;

    public List<Voucher> getAllVouchers(){
        return jdbc.query("select * from voucher", new RowMapper<Voucher>() {
            @Override
            public Voucher mapRow(ResultSet rs, int rowNum) throws SQLException {
                Voucher voucher = new Voucher();
                voucher.setIdCostumer(rs.getInt("idCostumer"));
                voucher.setDiscount(rs.getInt("discount"));
                return voucher;
            }
        });
    }

    public int insertOrder(Voucher voucher){
        MapSqlParameterSource params = new MapSqlParameterSource();

        int max = getAutoIncrement();
        params.addValue("idCostumer", max);
        params.addValue("discount",0);
        jdbc.update("insert into voucher(idCostumer, discount) values (:idCostumer,:discount)",params);
        return max;
    }

    public List<Voucher> getVoucherById(int idVoucher) {
        MapSqlParameterSource params = new MapSqlParameterSource(
                "idVoucher", idVoucher);
        return jdbc.query(
                "select * from voucher where idCostumer = :idVoucher",
                params, new RowMapper<Voucher>() {

                    public Voucher mapRow(ResultSet rs, int rowNum)
                            throws SQLException {
                        Voucher voucher = new Voucher();
                        voucher.setDiscount(rs.getInt("discount"));
                        voucher.setIdCostumer(idVoucher);
                        return voucher;
                    }
                });

    }

    public void updateVoucher(Voucher voucher){
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("idCostumer",voucher.getIdCostumer());
        params.addValue("discount",voucher.getDiscount());
        jdbc.update("update voucher set discount=:discount where idCostumer=:idCostumer",params);
    }

    public int getAutoIncrement(){
        List<Voucher> vouchers= getAllVouchers();
        int max = 0;
        if(vouchers.size()>0){
            for(Voucher voucher:vouchers){
                if(voucher.getIdCostumer() > max){
                    max = voucher.getIdCostumer();
                }
            }
        }
        return max +1;
    }


    @Autowired
    public void setJdbc(NamedParameterJdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }
}

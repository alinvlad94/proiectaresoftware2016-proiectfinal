package controller.dao;

import controller.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Alin on 5/20/2016.
 */

@Component("userDao")
public class UserDao {
    private NamedParameterJdbcTemplate jdbc;

    @Autowired
    @Qualifier("dataSource")
    public void setDataSource(DataSource jdbc) {
        this.jdbc = new NamedParameterJdbcTemplate(jdbc);
    }

    public List<User> getAllUsers() {
        return jdbc.query("select * from users ", new RowMapper<User>() {
            public User mapRow(ResultSet rs, int rowNum) throws SQLException {

                User user = new User();
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setAuthority((getAuthority(rs.getString("username")).get(0).getAuthority()));
                return user;
            }
        });
    }

    public List<User> getByUsername(String username) {
        MapSqlParameterSource params = new MapSqlParameterSource("username",
                username);
        return jdbc.query("select * from users where username = :username",
                params, new RowMapper<User>() {

                    public User mapRow(ResultSet rs, int rowNum)
                            throws SQLException {
                        User user = new User();
                        user.setUsername(rs.getString("username"));
                        user.setPassword(rs.getString("password"));
                        user.setAuthority((getAuthority(rs.getString("username")).get(0).getAuthority()));
                        return user;
                    }
                });

    }

    public List<User> getAuthority(String username) {
        MapSqlParameterSource params = new MapSqlParameterSource("username",
                username);
        return jdbc.query("select * from authorities where username = :username",
                params, new RowMapper<User>() {

                    public User mapRow(ResultSet rs, int rowNum)
                            throws SQLException {
                        User user = new User();
                        user.setUsername(rs.getString("username"));
                        user.setAuthority(rs.getString("authority"));
                        return user;
                    }
                });

    }

    public void insertUser(User user){
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("password",user.getPassword());
        params.addValue("username", user.getUsername());
        params.addValue("authority", user.getAuthority());
        params.addValue("enabled",1);
        jdbc.update("insert into users(enabled,password,username) values (:enabled,:password,:username)",params);
        jdbc.update("insert into authorities(username, authority) values(:username,:authority)",params);

    }

    public void resetPassword(String password,String username){
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("password",password);
        params.addValue("username", username);
        jdbc.update("update users set password= :password where username= :username", params);
    }

    public void deleteUser(String username){
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("username",username);
        jdbc.update("delete from users where username=:username", params);
        jdbc.update("delete from authorities where username=:username",params);
    }
}

package controller.controllers;

import controller.dao.VoucherDao;
import controller.mail.Mail;
import controller.model.Voucher;
import controller.service.MailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Alin on 5/22/2016.
 */

@RestController
@RequestMapping("/mail")
public class MailController {

    VoucherDao voucherDao;
    MailSenderService mailSenderService;

    @RequestMapping(method= RequestMethod.POST, consumes="application/json")
    public String sendEmail(@RequestBody Mail mail){

        mail.setFrom("Alin Restaurant's");
        mail.setSubject("Your voucher");
        mail.setMessage("You voucher is : "+ voucherDao.getAutoIncrement());
        mailSenderService.sendEmail(mail);
        voucherDao.insertOrder(new Voucher());
        return "";
    }

    @Autowired
    public void setVoucherDao(VoucherDao voucherDao) {
        this.voucherDao = voucherDao;
    }

    @Autowired
    public void setMailSenderService(MailSenderService mailSenderService) {
        this.mailSenderService = mailSenderService;
    }
}

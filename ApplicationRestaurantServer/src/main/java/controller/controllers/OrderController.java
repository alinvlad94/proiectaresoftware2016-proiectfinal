package controller.controllers;

import controller.dao.OrderDao;
import controller.model.Order;
import controller.model.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Alin on 5/21/2016.
 */

@RestController
@RequestMapping("/orders")
public class OrderController {

    OrderDao orderDao;

    @RequestMapping(value="/add", method = RequestMethod.POST, consumes = "application/json")
    public String addOrder(@RequestBody Orders orders){
        for(Order order: orders.getOrders()){
            orderDao.insertOrder(order);
        }
        System.out.print(orders);
        return "Success";
    }

    @RequestMapping(value="/finishorder",method=RequestMethod.POST,consumes="application/json")
    public String finishOrder(@RequestBody Order order){
        orderDao.makeOrderUnactive(order.getTable_number());
        return "Success";
    }


    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public Orders sendAllOrders(){
        Orders orders = new Orders(orderDao.getAllOrders());
        return orders;
    }



    @Autowired
    public void setOrderDao(OrderDao orderDao) {
        this.orderDao = orderDao;
    }
}

package controller.controllers;

import controller.dao.TestDao;
import controller.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Alin on 5/20/2016.
 */
@RestController("helloController")
public class HelloController {

    private TestDao testDao;

    @Autowired
    public void setTestDao(TestDao testDao) {
        this.testDao = testDao;
    }

    @RequestMapping("/")
    public String testHome(){
        List<User> users = testDao.getAllUsers();
        StringBuilder str = new StringBuilder();
        for(User u : users){
            str.append(u.getUsername()).append("      ");
        }
        return str.toString();
    }

    @RequestMapping("/home")
    public String aa(){
        return "Spring Boot";
    }
}

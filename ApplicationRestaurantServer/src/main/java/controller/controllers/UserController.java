package controller.controllers;

import controller.dao.TestDao;
import controller.dao.UserDao;
import controller.model.User;
import controller.model.Users;
import org.eclipse.persistence.annotations.ReturnUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Alin on 5/20/2016.
 */

@RestController
@RequestMapping("/users")
public class UserController {

    private UserDao userDao;

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao= userDao;
    }

    @RequestMapping(method= RequestMethod.GET, produces="application/json")
    public Users getUsers(){
        Users users = new Users(userDao.getAllUsers());
        return users;
    }

    @RequestMapping(value="/insert",method=RequestMethod.POST, consumes = "application/json")
    public String insertUser(@RequestBody User user){
        userDao.insertUser(user);
        return "Success";
    }

    @RequestMapping(value="/update", method=RequestMethod.POST,consumes="application/json")
    public String updateUser(@RequestBody User user){
        userDao.resetPassword(user.getPassword(), user.getUsername());
        return "success";
    }

    @RequestMapping(value="/delete", method=RequestMethod.DELETE)
    public String deleteUser(@RequestParam("username") String username){
        userDao.deleteUser(username);
        return "Success";
    }

    @RequestMapping(value="/user", method = RequestMethod.GET, produces = "application/json")
    public User getUserWithUsername(@RequestParam("username") String username){
        User user = userDao.getByUsername(username).get(0);
        return user;
    }
}

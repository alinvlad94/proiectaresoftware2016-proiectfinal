package controller.controllers;

import controller.dao.VoucherDao;
import controller.model.Voucher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Alin on 5/23/2016.
 */

@RestController
@RequestMapping("/voucher")
public class VoucherController {

    VoucherDao voucherDao;

    @RequestMapping("/valid")
    public String isVoucherValid(@RequestBody Voucher voucher){
        List<Voucher> vouchers = voucherDao.getAllVouchers();
        for(Voucher v : vouchers){
            if(v.getIdCostumer() == voucher.getIdCostumer()){
                return "yes";
            }
        }
        return "no";
    }

    @RequestMapping("/update")
    public String updateVoucher(@RequestBody Voucher voucher){
        voucherDao.updateVoucher(voucher);
        return "Success";
    }

    @RequestMapping(value="/idVoucher", produces="application/json",method= RequestMethod.GET)
    public Voucher getById(@RequestParam("idVoucher") int idVoucher){
        return voucherDao.getVoucherById(idVoucher).get(0);
    }

    @Autowired
    public void setVoucherDao(VoucherDao voucherDao) {
        this.voucherDao = voucherDao;
    }
}

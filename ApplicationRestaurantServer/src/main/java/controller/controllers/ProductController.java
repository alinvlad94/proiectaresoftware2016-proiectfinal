package controller.controllers;

import controller.dao.ProductDao;
import controller.model.Product;
import controller.model.Products;
import controller.model.Users;
import oracle.jrockit.jfr.openmbean.ProducerDescriptorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * Created by Alin on 5/21/2016.
 */

@RestController
@RequestMapping("/products")
public class ProductController {

    private ProductDao productDao;

    @RequestMapping(method= RequestMethod.GET, produces="application/json")
    public Products getUsers(){
        Products products = new Products(productDao.getAllProducts());
        return products;
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteProduct(@RequestParam("idProduct") int idProduct){
        productDao.deleteProduct(idProduct);
    }

    @RequestMapping(value="/update", consumes="application/json", method=RequestMethod.POST)
    public String updateProduct(@RequestBody Product product){
        productDao.updateProduct(product);
        return "Success";
    }

    @RequestMapping(value="/insert",consumes="application/json", method = RequestMethod.POST)
    public String insertProduct(@RequestBody Product product){
        productDao.insertProduct(product);
        return "Success";
    }

    @Autowired
    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }
}

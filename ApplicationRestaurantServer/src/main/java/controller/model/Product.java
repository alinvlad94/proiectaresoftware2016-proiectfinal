package controller.model;

/**
 * Created by Alin on 5/20/2016.
 */

public class Product {

    private int idProduct;
    private String name;
    private int price;
    private String description;
    private String photo;
    private String category;

    public Product() {
    }

    public Product(int idProduct, String name, int price, String description, String photo, String category) {
        this.idProduct = idProduct;
        this.name = name;
        this.price = price;
        this.description = description;
        this.photo = photo;
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}

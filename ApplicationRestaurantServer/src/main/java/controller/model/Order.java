package controller.model;

/**
 * Created by Alin on 5/20/2016.
 */
public class Order {

    private int idOrder;
    private int idProdus;
    private String name;
    private int pieces;
    private int table_number;
    private int state_activ;
    private String waitress_name;
    private int idVoucher;
    private int price;

    public Order() {
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public int getIdProdus() {
        return idProdus;
    }

    public void setIdProdus(int idProdus) {
        this.idProdus = idProdus;
    }

    public int getPieces() {
        return pieces;
    }

    public void setPieces(int pieces) {
        this.pieces = pieces;
    }

    public int getTable_number() {
        return table_number;
    }

    public void setTable_number(int table_number) {
        this.table_number = table_number;
    }

    public int getState_activ() {
        return state_activ;
    }

    public void setState_activ(int state_activ) {
        this.state_activ = state_activ;
    }

    public String getWaitress_name() {
        return waitress_name;
    }

    public void setWaitress_name(String waitress_name) {
        this.waitress_name = waitress_name;
    }

    public int getIdVoucher() {
        return idVoucher;
    }

    public void setIdVoucher(int idVoucher) {
        this.idVoucher = idVoucher;
    }
}


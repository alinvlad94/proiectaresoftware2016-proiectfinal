package controller.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alin on 5/20/2016.
 */
public class Users {

    private List<User> users;

    public Users() {
    }

    public Users(List<User> users) {
        this.users = users;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}

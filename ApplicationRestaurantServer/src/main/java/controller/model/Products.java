package controller.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alin on 5/20/2016.
 */
public class Products {

    private List<Product> products;

    public Products() {
    }

    public Products(List<Product> products) {
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }
}

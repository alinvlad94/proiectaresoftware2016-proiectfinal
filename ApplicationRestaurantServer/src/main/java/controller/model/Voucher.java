package controller.model;

/**
 * Created by Alin on 5/20/2016.
 */
public class Voucher {

    private int idCostumer;
    private int discount;

    public Voucher() {
    }

    public Voucher(int idCostumer, int discount) {
        this.idCostumer = idCostumer;
        this.discount = discount;
    }

    public int getIdCostumer() {
        return idCostumer;
    }

    public void setIdCostumer(int idCostumer) {
        this.idCostumer = idCostumer;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }
}

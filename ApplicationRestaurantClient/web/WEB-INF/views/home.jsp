
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Alin
  Date: 5/19/2016
  Time: 12:12 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<c:url value="css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="css/home.css"/>" rel="stylesheet">
    <script src="<c:url value="js/jquery.js"/>"></script>
    <script src="<c:url value="js/bootstrap.js"/>"></script>
    <title>Restaurant</title>
</head>
<body style="background: url(images/hugo.jpg) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;">
<ul class="nav nav-tabs">
    <li role="presentation" class="active"><a href="#">Home</a></li>
    <sec:authorize access="isAnonymous()"><li role="presentation"><a style="color:white"  href="#" data-toggle="modal" data-target="#login-modal">Login</a></li> </sec:authorize>
    <sec:authorize access="isAuthenticated()"><li  role="presentation"><a style="color:white" href="client/j_spring_security_logout">Logout</a></li> </sec:authorize>
</ul>

<sec:authorize access="isAuthenticated()"></sec:authorize>
<div style="margin-top:3%;" class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="loginmodal-container">
            <h1 style="color:white" >Login to Your Account</h1><br>
            <form action="client/j_spring_security_check" method="post">
                <input type="text" name="j_username" placeholder="Username">
                <input type="password" name="j_password" placeholder="Password">
                <input type="submit" name="login" class="login loginmodal-submit" value="Login">
                <input type="hidden" name="${_csrf.parameterName}"
                       value="${_csrf.token}" />

            </form>

            <div class="login-help">
                <a href="#">Register</a> - <a href="#">Forgot Password</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>

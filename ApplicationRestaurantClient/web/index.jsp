<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Alin
  Date: 5/19/2016
  Time: 12:05 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="<c:url value="css/bootstrap.min.css"/>" rel="stylesheet">
      <link href="<c:url value="css/home.css"/>" rel="stylesheet">
      <script src="<c:url value="js/jquery.js"/>"></script>
      <script src="<c:url value="js/bootstrap.js"/>"></script>
    <title>Restaurant</title>
  </head>
  <body style="background: url(images/hugo.jpg) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;">

   <div class="container">
      <div class="well" style="opacity: 0.9; margin-top:3%; box-shadow: 0px 2px 2px 0px #FFFFFF; height:100%">
          <div class="jumbotron" style="opacity:1;">
              <center><h1>Bun venit la Alin's Restaurant</h1></center>
              <center><p>Va uram pofta buna!</p></center>
              <center>

                  <form action="enter" method="get">
                  <center><p style="margin-top:7%;color:#000000 ; font-size:16px; float:top">Selectati masa</p></center>
                  <center><select name="numar_masa">
                      <option value="Selectati masa" disabled>Selectati masa</option>
                      <option value="1">Masa 1</option>
                      <option value="2">Masa 2</option>
                      <option value="3">Masa 3</option>
                  <center><input class="button" type="submit"></input></center>
                  </select>
                  </center>
              </center>
          </div>
      </div>

   </div>

  </body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: Alin
  Date: 5/20/2016
  Time: 11:28 AM
  To change this template use File | Settings | File Templates.
--%>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<c:url value="/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/css/client.css"/>" rel="stylesheet">
    <script src="<c:url value="/js/jquery.js"/>"></script>
    <script src="<c:url value="/js/bootstrap.js"/>"></script>
    <title>Alin's Restaurant</title>
</head>
<body style="background: url(images/hugo.jpg) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;">

<div class="container">
    <div class="well" style="opacity: 0.9; margin-top:3%; box-shadow: 0px 2px 2px 0px #FFFFFF; height:100%">
      <div class="jumbotron">
          <center><div class="btn-group-lg" role="group" aria-label="...">
              <button onclick="location.href='category?category=pizza';" type="button" class="btn btn-default">Pizza</button>
              <button onclick="location.href='category?category=pui';" type="button" class="btn btn-default">Pui</button>
              <button onclick="location.href='category?category=ciorbe';" type="button" class="btn btn-default">Ciorbe</button>
              <button onclick="location.href='category?category=garnituri';" type="button" class="btn btn-default">Garnituri</button>
              <button onclick="location.href='category?category=racoritoare';" type="button" class="btn btn-default">Racoritoare</button>
      </div>
          </center>

        <c:forEach items="${products}" var="product">
            <div class="jumbotron">
                <img style="float:left" src="${product.photo}" class="img-thumbnail" alt="Cinque Terre" width="130" height="90">
                <div style="float:left;margin-left:15px; font-size:18px;">
                    <a>${product.name}</a><br>
                    <a>${product.price}</a><br>
                    <a>${product.description}</a>
                    </div>
            </div>
            <form style="float:right" action="addProduct" method="get">
                <input type="hidden" name="idProduct" value="${product.idProduct}" />
                <input type="text" name="quantity" placeholder="Cantitate" required/>
                <input type="submit" value="Adauga"/>
            </form>
        </c:forEach>



        <div class="jumbotron" style="opacity:1; margin-top:100px;">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                    <c:forEach items="${orders}" var="order">
                        <tr>
                            <td>${order.name}</td>
                            <td>${order.pieces}</td>
                            <td>${order.price}</td>
                        </tr>
                    </c:forEach>

                </tbody>
            </table>
            <button onclick="location.href='deleteOrder';" type="button" class="btn btn-info">Delete</button>
            <button onclick="location.href='orderWithoutVoucher';" type="button" class="btn btn-info">Order without voucher</button>
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#login-modal">Order with voucher</button>
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#email-modal">Get a voucher yourself</button>
            <c:if test="${not empty errors}">
                <a style="color:red; font-size:18px">${errors}</a>
            </c:if>
        </div>

    </div>

</div>

    <div style="margin-top:3%;" class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Voucher</h4>
                </div>
                <form action="order">
                <div class="modal-body">
                    <center><input type="text" name="idVoucher" placeholder="VOUCHER NUMBER" required></center>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Submit Order</button>
                </div>
                </form>
            </div>
        </div>
    </div>


    <div style="margin-top:3%;" class="modal fade" id="email-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Enter a email so we can send you a voucher code</h4>
                </div>
                <form action="email">
                    <div class="modal-body">
                        <center><input type="text" name="email" placeholder="Your Email" required></center>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default">Send voucher</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    </div>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: Alin
  Date: 5/22/2016
  Time: 12:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<c:url value="/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/css/waitress.css"/>" rel="stylesheet">
    <script src="<c:url value="/js/jquery.js"/>"></script>
    <script src="<c:url value="/js/bootstrap.js"/>"></script>
    <title>Alin's Restaurant</title>
</head>
<body style="background: url(images/hugo.jpg) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;">

<div class="container">
    <div class="well" style="opacity: 0.9; margin-top:3%; box-shadow: 0px 2px 2px 0px #FFFFFF; height:100%">
    <div class="jumbotron" style="margin-top:3%; opacity:0.9;">

        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="#">Home</a></li>
            <sec:authorize access="isAuthenticated()"><li  role="presentation"><a style="color:black" href="client/j_spring_security_logout">Logout</a></li> </sec:authorize>
        </ul>
        <div style="float:right">
        <button onclick="location.href='table_number?table_number=1';" type="button" class="btn btn-default">Masa 1</button>
        <button onclick="location.href='table_number?table_number=2';" type="button" class="btn btn-default">Masa 2</button>
        <button onclick="location.href='table_number?table_number=3';" type="button" class="btn btn-default">Masa 3</button>
        </div>
    </div>
    <div class="jumbotron" style="margin-top:3%; opacity:0.9;">
           <table class="table table-striped">
               <thead>
               <tr>
                   <th>Product</th>
                   <th>Quantity</th>
                   <th>Price</th>
               </tr>
               </thead>
               <tbody>

              <c:forEach items="${comandedOrders}" var="order">
                   <tr>
                       <td>${order.name}</td>
                       <td>${order.pieces}</td>
                       <td>${order.price}</td>
                   </tr>
              </c:forEach>

               </tbody>
           </table>
        </div>
        <div class="jumbotron">
            <button onclick="location.href='finishorder';" type="button" class="btn btn-info"  >Get Paycheck</button>
        </div>
</div>
    </div>


</body>
</html>

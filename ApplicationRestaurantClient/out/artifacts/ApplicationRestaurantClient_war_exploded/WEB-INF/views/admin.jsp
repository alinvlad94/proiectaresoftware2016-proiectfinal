<%--
  Created by IntelliJ IDEA.
  User: Alin
  Date: 5/20/2016
  Time: 11:28 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<c:url value="/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/css/admin.css"/>" rel="stylesheet">
    <script src="<c:url value="/js/jquery.js"/>"></script>
    <script src="<c:url value="/js/bootstrap.js"/>"></script>
    <title>Alin's Restaurant</title> <title>Title</title>
</head>
<body style="background: url(images/hugo.jpg) no-repeat center center fixed;
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
background-size: cover;">

<div class="container">
    <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li><a href="j_spring_security_logout" id="productsButton">Logout</a></li>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>

</div>

<div class="container">
    <div class="well" style="margin-top:2%;  opacity: 0.9;">
        <div class="jumbotron" style="height:1500px;">
            <div style="float:left; width: 45%; height:1500px ;border: 1px black;">
                <form  role="form" action="get">
                    <div  style="margin-bottom:40px;" class = "form-group">
                        <label for="idProduct">Select product</label>
                        <select multiple class="form-control" name ="idProduct" id="idProduct" required>
                         <c:forEach items="${products}" var="product">
                             <option value="${product.idProduct}">${product.name}</option>
                         </c:forEach>
                        </select>
                        <input style="margin-top:20px" class="btn btn-info" formaction="deleteProduct"
                               name="submit"  type="submit" value="Delete" />
                        <input style="margin-top:20px" class="btn btn-info" formaction="updateProduct"
                               name="submit"  type="submit" value="Update" />
                        <button  style="margin-top:20px" onclick="location.href='getInsert';" type="button" class="btn btn-info">Add</button>
                    </div>
                </form>

                <c:if test="${not empty activeUpdate}">
                    <form action="updateProductGo" method="post">
                        <input style = "margin-bottom:15px;" type="text" name="name" value="${updatedProduct.name}"/><br>
                        <input style = "margin-bottom:15px;" type="text" name="price" value="${updatedProduct.price}"><br>
                        <input style = "margin-bottom:15px;" type="text" name="description" value="${updatedProduct.description}"><br>
                        <input style = "margin-bottom:15px;" type="text" name="category" value="${updatedProduct.category}"/><br>
                        <input type="hidden" name="idProduct" value="${updatedProduct.idProduct}"/>
                        <input type="hidden" name="photo" value="${updatedProduct.photo}"/>
                        <input type="submit" class="btn btn-info" value="Update Product"/>
                    </form>
                </c:if>

                <c:if test="${not empty activeInsert}">
                    <form action="insertProduct" method="post" enctype="multipart/form-data">
                        <input style = "margin-bottom:15px;" type="text" name="name"  placeholder="Name"/><br>
                        <input style = "margin-bottom:15px;" type="text" name="price"  placeholder="Price"><br>
                        <input style = "margin-bottom:15px;" type="text" name="description"  placeholder="Description"><br>
                        <input style = "margin-bottom:15px;" type="text" name="category"  placeholder="Category"/><br>
                        <input type="file" name="photo"/>
                        <input type="submit" class="btn btn-info" value="Insert Product"/>
                    </form>
                </c:if>












            </div>


            <div style="float:right;width: 45%; height:1500px;border: 1px black;">

                <form  role="form" action="get">
                    <div  style="margin-bottom:40px;" class = "form-group">
                        <label for="idProduct">Select User</label>
                        <select multiple class="form-control" name ="username" id="username" required>
                            <c:forEach items="${users}" var="user">
                                <option value="${user.username}">${user.username}</option>
                            </c:forEach>
                        </select>
                        <input style="margin-top:20px" class="btn btn-info" formaction="deleteUser"
                               name="submit"  type="submit" value="Delete" />
                        <input style="margin-top:20px" class="btn btn-info" formaction="updateUser"
                               name="submit"  type="submit" value="Update" />
                        <button  style="margin-top:20px" onclick="location.href='getInsertUser';" type="button" class="btn btn-info">Add</button>
                    </div>
                </form>

                <c:if test="${not empty activeUpdateUser}">
                    <form action="updateUserGo" method="post">
                        <input style = "margin-bottom:15px;" type="text" name="username" value="${updateUser.username}" readonly/><br>
                        <input style = "margin-bottom:15px;" type="text" name="password" value="${updateUser.password}"><br>
                        <input type="hidden" name="authority" value="${updateUser.authority}"/>
                        <input type="submit" class="btn btn-info" value="Update User"/>
                    </form>
                </c:if>

                <c:if test="${not empty activeInsertUser}">
                    <form action="insertUserGo" method="post">
                        <input style = "margin-bottom:15px;" type="text" name="username"  placeholder="Username"/><br>
                        <input style = "margin-bottom:15px;" type="text" name="password"  placeholder="Password"><br>
                        <input type="submit" class="btn btn-info" value="Insert User"/>
                    </form>
                </c:if>

            </div>
        </div>

    </div>

</div>

</div>


</body>
</html>

package com.utils;


import com.model.Orders;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;


/**
 * Created by Alin on 4/26/2016.
 */

@Service("convertorService")
public class ConvertorService {

    public String convertLisOfParticipantstToXML(Orders orders) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Orders.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);


        //Marshal the orders list in file
        jaxbMarshaller.marshal(orders, new File("E:/ECLIPSE EE/workspace/ApplicationRestaurantClient/web/WEB-INF/resources/paycheck/paycheck.xml"));
        return "E:/ECLIPSE EE/workspace/ApplicationRestaurantClient/web/WEB-INF/resources/paycheck/paycheck.xml";
    }

    public String convertListOfParticipantsToJSON(Orders orders) throws JAXBException {

        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");

        JAXBContext jaxbContext = JAXBContext.newInstance(Orders.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        jaxbMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, false);
        //Marshal the orders list in file
        jaxbMarshaller.marshal(orders,new File("E:/ECLIPSE EE/workspace/ApplicationRestaurantClient/web/WEB-INF/resources/paycheck/paycheck.json"));
        return "E:/ECLIPSE EE/workspace/ApplicationRestaurantClient/web/WEB-INF/resources/paycheck/paycheck.json";
    }

}

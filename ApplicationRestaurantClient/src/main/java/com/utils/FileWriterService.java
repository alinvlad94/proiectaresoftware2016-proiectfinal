package com.utils;

import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Alin on 5/22/2016.
 */

public class FileWriterService {

    public static void generatePaycheckWithVoucher(String text) {

        try {


            File file = new File("E:/ECLIPSE EE/workspace/ApplicationRestaurantClient/web/WEB-INF/resources/paycheck/paycheck.txt");
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(text);
            bw.close();

        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public static void generatePaycheckWithoutVoucher(String text) {

        try {


            File file = new File("E:/ECLIPSE EE/workspace/ApplicationRestaurantClient/web/WEB-INF/resources/paycheck/paycheck.txt");
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(text);
            bw.close();

        }catch(IOException e){
            e.printStackTrace();
        }
    }
}

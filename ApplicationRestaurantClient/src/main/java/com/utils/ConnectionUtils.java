package com.utils;

import com.model.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Alin on 5/20/2016.
 */
public class ConnectionUtils {

    //Email
    public static void sendEmailWithNewVoucher(Mail mail){

        JSONObject request = new JSONObject(mail);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String uri = "http://localhost:8085/mail";
        ResponseEntity<String> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, String.class);
        System.out.print(response);
    }

    //Products
    public static Products sendGetForListOfProducts(){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String uri = "http://localhost:8085/products";
        ResponseEntity<Products> products = restTemplate
                .getForEntity(uri,Products.class);

        return products.getBody();
    }

    public static void deleteProduct(int idProduct){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        String uri = "http://localhost:8085/products?idProduct="+idProduct;
        restTemplate.delete(uri);
    }

    public static void updateProduct(Product product){
        JSONObject json = new JSONObject(product);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(json.toString(), headers);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String uri = "http://localhost:8085/products/update";
        ResponseEntity<String> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, String.class);
        System.out.print(response);
    }

    public static void insertProduct(Product product){
        JSONObject json = new JSONObject(product);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(json.toString(), headers);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String uri = "http://localhost:8085/products/insert";
        ResponseEntity<String> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, String.class);
        System.out.print(response);
    }

    //Orders
    public static void sendPostWithListOfOrders(Orders orders){

        JSONObject request = new JSONObject(orders);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String uri = "http://localhost:8085/orders/add";
        ResponseEntity<String> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, String.class);
        System.out.print(response);
    }

    public static List<Order> sendGetForListOfOrders(){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String uri = "http://localhost:8085/orders";
        ResponseEntity<Orders> orders = restTemplate
                .getForEntity(uri,Orders.class);

        return orders.getBody().getOrders();
    }

    public static void getPaycheckOrders(int table_number){
        Order order = new Order();
        order.setTable_number(table_number);
        JSONObject json = new JSONObject(order);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(json.toString(), headers);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String uri = "http://localhost:8085/orders/finishorder";
        ResponseEntity<String> response = restTemplate
               .exchange(uri, HttpMethod.POST, entity, String.class);
        System.out.print(response);
    }

    //Users
    public static List<User> sendGetForListOfUsers(){

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        String uri = "http://localhost:8085/users";
        ResponseEntity<Users> users = restTemplate
                .getForEntity(uri,Users.class);
        return users.getBody().getUsers();
    }

    public static void insertUser(User user){
        JSONObject json = new JSONObject(user);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(json.toString(), headers);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String uri = "http://localhost:8085/users/insert";
        ResponseEntity<String> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, String.class);
        System.out.print(response);
    }

    public static void updateUser(User user){
        JSONObject json = new JSONObject(user);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(json.toString(), headers);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String uri = "http://localhost:8085/users/update";
        ResponseEntity<String> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, String.class);
        System.out.print(response);
    }

    public static void deleteUser(String username){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        String uri = "http://localhost:8085/users/delete?username="+username;
        restTemplate.delete(uri);
    }

    public static User getUserByUsername(String username){
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        String uri = "http://localhost:8085/users/user?username="+username;
        ResponseEntity<User> user = restTemplate
                .getForEntity(uri,User.class);
        return user.getBody();
    }


    //Voucher
    public static boolean isVoucherActive(int idVoucher){
        Voucher voucher = new Voucher();
        voucher.setDiscount(0);
        voucher.setIdCostumer(idVoucher);

        JSONObject json = new JSONObject(voucher);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(json.toString(), headers);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String uri = "http://localhost:8085/voucher/valid";
        ResponseEntity<String> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, String.class);
        if(response.getBody().equals("yes")){
            return true;
        }else{
            return false;
        }
    }

    public static void updateVoucher(Voucher voucher){
        JSONObject json = new JSONObject(voucher);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(json.toString(), headers);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String uri = "http://localhost:8085/voucher/update";
        ResponseEntity<String> response = restTemplate
                .exchange(uri, HttpMethod.POST, entity, String.class);
    }

    public static Voucher sendGetForVoucherWithId(int idVoucher){

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        String uri = "http://localhost:8085/voucher/idVoucher?idVoucher="+idVoucher;
        ResponseEntity<Voucher> users = restTemplate
                .getForEntity(uri,Voucher.class);
        return users.getBody();
    }
}

package com.model;

import java.util.ArrayList;

/**
 * Created by Alin on 5/20/2016.
 */
public class Users {

    private ArrayList<User> users;

    public Users() {
    }

    public Users(ArrayList<User> users) {
        this.users = users;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }
}

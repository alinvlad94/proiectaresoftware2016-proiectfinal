package com.controller;

import com.model.User;
import com.utils.ConnectionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.View;

import javax.ws.rs.Consumes;
import java.util.List;

/**
 * Created by Alin on 5/19/2016.
 */

@Controller
public class HomeController {

    @RequestMapping("/home")
    public String getHome(){
        return "home";
    }

    @RequestMapping("/client")
    public String getClientPage(){
        return "client";
    }
    @RequestMapping("/test")
    public String testRestController(Model model){
        List<User> users = ConnectionUtils.sendGetForListOfUsers();
        model.addAttribute("message","Merge");
        return "client";
    }
}

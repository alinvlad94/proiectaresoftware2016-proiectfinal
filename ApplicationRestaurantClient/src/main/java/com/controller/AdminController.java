package com.controller;

import com.model.Product;
import com.model.User;
import com.utils.ConnectionUtils;
import com.utils.FileUploaderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alin on 5/20/2016.
 */

@Controller
public class AdminController {

    @RequestMapping("/admin")
    public String getAdminLandPage(HttpSession session){
        session.setAttribute("products",ConnectionUtils.sendGetForListOfProducts().getProducts());
        session.setAttribute("users",selectWithRoleUSER(ConnectionUtils.sendGetForListOfUsers()));
        return "admin";
    }

    @RequestMapping("/deleteProduct")
    public String getAdminWithInsert(@RequestParam("idProduct") int idProduct,Model model){
        ConnectionUtils.deleteProduct(idProduct);
        System.out.print(idProduct);
        return "redirect:/admin";
    }

    @RequestMapping("/updateProduct")
    public String getAdminWithUpdate(@RequestParam("idProduct") int idProduct, Model model){
        model.addAttribute("updatedProduct",getProdById(idProduct));
        model.addAttribute("activeUpdate","a");
        System.out.print(idProduct);
        return "admin";
    }

    @RequestMapping("/updateUser")
    public String getUserUpdate(@RequestParam("username") String username, Model model){
        model.addAttribute("updateUser",ConnectionUtils.getUserByUsername(username));
        model.addAttribute("activeUpdateUser","a");
        return "admin";
    }

    @RequestMapping("/getInsert")
    public String getAdminWithDelete(Model model){
        model.addAttribute("activeInsert","a");
        return "admin";
    }

    @RequestMapping("/insertProduct")
    public String insertProduct(@RequestParam("name")String name, @RequestParam("category")String category, @RequestParam("description")String description,
                                @RequestParam("price")int price, @RequestParam("photo") MultipartFile[] photo, Model model){
        String photo_root = FileUploaderService.uploadPhoto(photo);
        Product product = new Product(0,name,price,description,photo_root,category);
        ConnectionUtils.insertProduct(product);
        return "redirect:/admin";
    }

    @RequestMapping(value = "/updateProductGo")
    public String getUpdateProduct(@RequestParam("idProduct")int idProduct,@RequestParam("name")String name, @RequestParam("category")String category, @RequestParam("description")String description,
                                   @RequestParam("photo")String photo, @RequestParam("price")int price, Model model){
        Product product = new Product(idProduct,name,price,description,photo,category);
            ConnectionUtils.updateProduct(product);
        return "redirect:/admin";
    }

    @RequestMapping("/getInsertUser")
    public String getUserDeletePage(Model model){
        model.addAttribute("activeInsertUser","a");
        return "admin";
    }

    @RequestMapping(value="/updateUserGo", method = RequestMethod.POST)
    public String updateUser(@RequestParam("username")String username, @RequestParam("password")String password, @RequestParam("authority")String authority){
        User user = new User(username, password, authority);
        ConnectionUtils.updateUser(user);
        return "redirect:/admin";
    }

    @RequestMapping(value="/insertUserGo", method = RequestMethod.POST)
    public String insertUser(@RequestParam("username")String username, @RequestParam("password")String password){
        User user = new User(username, password, "ROLE_USER");
        ConnectionUtils.insertUser(user);
        return "redirect:/admin";
    }

    @RequestMapping(value="/deleteUser")
    public String deleteUser(@RequestParam("username") String username){
        ConnectionUtils.deleteUser(username);
        return "redirect:/admin";
    }


    public Product getProdById(int idProduct){
        for(Product product : ConnectionUtils.sendGetForListOfProducts().getProducts()){
            if(product.getIdProduct()==idProduct){
                return product;
            }
        }
        return new Product();
    }

    public List<User> selectWithRoleUSER(List<User> users){
        ArrayList<User> selectedUsers = new ArrayList<User>();
        for(User user : users){
           if(user.getAuthority().equals("ROLE_USER")){
               selectedUsers.add(user);
           }
        }
        return selectedUsers;
    }
}

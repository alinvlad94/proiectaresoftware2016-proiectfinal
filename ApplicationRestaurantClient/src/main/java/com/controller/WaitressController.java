package com.controller;

import com.model.Order;
import com.model.Orders;
import com.model.Voucher;
import com.utils.ConnectionUtils;
import com.utils.ConvertorService;
import com.utils.FileWriterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.support.ContextExposingHttpServletRequest;

import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alin on 5/20/2016.
 */

@Controller
public class WaitressController {


    ConvertorService convertorService = new ConvertorService();

    @RequestMapping("/waitress")
    public String getLandPage(HttpSession session) {
        session.setAttribute("table_number", 1);
        List<Order> orders = getOrdersActiveOnTable(1);
        session.setAttribute("comandedOrders", orders);
        return "waitress";
    }

    @RequestMapping("/table_number")
    public String getTableNumber(@RequestParam("table_number") int table_number, HttpSession session) {
        session.setAttribute("table_number", table_number);
        List<Order> orders = getOrdersActiveOnTable(table_number);
        session.setAttribute("comandedOrders", orders);
        return "waitress";
    }

    @RequestMapping(value = "/finishorder")
    public String finishOrder(HttpSession session) {
        int table_number = (Integer) session.getAttribute("table_number");
        List<Order> activeOrders = getOrdersActiveOnTable(table_number);

        Orders orders = new Orders(activeOrders);
        if(ConnectionUtils.isVoucherActive(orders.getOrders().get(0).getIdVoucher())) {
            addDiscountToUser(calculateDiscout(calculateTotalSum(orders)), orders);
        }
        generatePaycheckWithDiscount(orders);

        try {
            convertorService.convertLisOfParticipantstToXML(orders);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        ConnectionUtils.getPaycheckOrders(table_number);
        System.out.print(table_number);
        return "waitress";
    }

    private List<Order> getOrdersActiveOnTable(int table_number) {
        List<com.model.Order> orders = ConnectionUtils.sendGetForListOfOrders();
        ArrayList<Order> list = new ArrayList<Order>();
        for (Order order : orders) {
            if (order.getTable_number() == table_number && order.getState_activ() == 1) {
                list.add(order);
            }
        }
        return list;
    }

    public int calculateTotalSum(Orders orders) {
        int sum = 0;
        for (Order order : orders.getOrders()) {
            sum += order.getPrice();
        }
        return sum;
    }

    public int calculateDiscout(int sum) {
        if (sum > 50 && sum <= 200) {
            return 1;
        } else if (sum > 200) {
            return 2;
        } else {
            return 0;
        }
    }

    public void addDiscountToUser(int discount, Orders orders) {
        int idVoucher = orders.getOrders().get(0).getIdVoucher();
        discount = discount + ConnectionUtils.sendGetForVoucherWithId(idVoucher).getDiscount();
        Voucher voucher = new Voucher();
        voucher.setIdCostumer(idVoucher);
        voucher.setDiscount(discount);
        ConnectionUtils.updateVoucher(voucher);
    }

    public void generatePaycheckWithDiscount(Orders orders){
        int idVoucher = orders.getOrders().get(0).getIdVoucher();
        if(ConnectionUtils.isVoucherActive(orders.getOrders().get(0).getIdVoucher())) {
            Voucher v = ConnectionUtils.sendGetForVoucherWithId(idVoucher);
            int discount = v.getDiscount();
            int sum = calculateTotalSum(orders);
            StringBuilder str = new StringBuilder();
            for (Order order : orders.getOrders()) {
                str.append(order.getName()).append("   ").append(order.getPrice()).append("\n");
            }
            double sumDiscounted = sum * discount / 100.0;
            double sumWithDiscout = sum - sumDiscounted;
            String text = "Your total sum that you have to pay is :" + sumWithDiscout + "\n" + " At this sum was credited a discout of " + discount + "%" + " that equals :" + sumDiscounted + " thanks to your voucher :" + idVoucher;
            str.append(text);
            FileWriterService.generatePaycheckWithVoucher(str.toString());
        }
        else{
            StringBuilder str = new StringBuilder();
            int sum = calculateTotalSum(orders);
            for (Order order : orders.getOrders()) {
                str.append(order.getName()).append("   ").append(order.getPrice()).append("\n");
            }
            String text = "Your total sum that you have to pay is : "+sum;
            str.append(text);
            FileWriterService.generatePaycheckWithoutVoucher(str.toString());
        }
    }
}

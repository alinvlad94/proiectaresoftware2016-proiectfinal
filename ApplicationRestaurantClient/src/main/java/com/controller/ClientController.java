package com.controller;

import com.model.*;
import com.utils.ConnectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.support.ContextExposingHttpServletRequest;

import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alin on 5/20/2016.
 */

@Controller
public class ClientController {

    private List<Product> allProducts;
    private List<Order> sessionOrder = new ArrayList<Order>();
    private String selectedCategory = "pizza";


    @RequestMapping("/enter")
    public String enterSite(@RequestParam("numar_masa") int numar_masa, Model model, HttpSession session){
        session.setAttribute("numar_masa", numar_masa);
        allProducts = ((Products) ConnectionUtils.sendGetForListOfProducts()).getProducts();
        ArrayList<Product> prods = getByCategory("pizza");
        session.setAttribute("products",prods);
        return "client";
    }

    @RequestMapping(value="/addProduct", method = RequestMethod.GET)
    public String addProduct(@RequestParam("idProduct") int idProduct, @RequestParam("quantity") int quantity, HttpSession session){
        Order order = new Order();
        order.setName(getById(idProduct).getName());
        order.setPieces(quantity);
        order.setPrice(getById(idProduct).getPrice()*quantity);
        order.setName(getById(idProduct).getName());
        order.setIdProdus(idProduct);
        order.setIdVoucher(0);
        order.setTable_number((Integer) session.getAttribute("numar_masa"));
        order.setState_activ(1);
        sessionOrder.add(order);
        session.setAttribute("orders",sessionOrder);
        return "client";
    }

    @RequestMapping("/category")
    @GET
    public String enterSiteWithCategory(@RequestParam("category") String category, HttpSession session){
        session.setAttribute("products",getByCategory(category));
        return "client";
    }

    @RequestMapping("/order")
    @GET
    public String orderProductsWithVoucher (@RequestParam("idVoucher") int idVoucher, HttpSession session, Model model){
        if(ConnectionUtils.isVoucherActive(idVoucher)){
            for(Order order : sessionOrder){
                order.setIdVoucher(idVoucher);
            }
            Orders orders = new Orders(sessionOrder);
            ConnectionUtils.sendPostWithListOfOrders(orders);
            sessionOrder.clear();
            session.setAttribute("orders",new ArrayList<Order>());
            System.out.println(idVoucher);
            return "client";

        }else{
            model.addAttribute("errors","This voucher number does not exist");
            return "client";
        }


    }

    @RequestMapping("/orderWithoutVoucher")
    @GET
    public String orderProducts ( HttpSession session){
        Orders orders = new Orders(sessionOrder);
        ConnectionUtils.sendPostWithListOfOrders(orders);
        sessionOrder.clear();
        session.setAttribute("orders",new ArrayList<Order>());

        return "client";
    }

    @RequestMapping("/deleteOrder")
    public String deleteOrders(HttpSession session){
        sessionOrder.clear();
        session.setAttribute("orders", new ArrayList<Order>());
        return "client";
    }

    @RequestMapping("/email")
    @GET
    public String sendVoucherToEmail (@RequestParam("email") String email, HttpSession session){

        Mail mail = new Mail();
        mail.setTo(email);
        ConnectionUtils.sendEmailWithNewVoucher(mail);
        return "client";
    }


    public ArrayList<Product> getByCategory(String category){
        ArrayList<Product> products = new ArrayList<Product>();
        for(Product product : allProducts){
             if(product.getCategory().equals(category)){
                 products.add(product);
             }
        }
        return products;
    }

    public Product getById(int idProduct){
        ArrayList<Product> products = new ArrayList<Product>();
        for(Product product : allProducts){
            if(product.getIdProduct()==idProduct){
                return product;
            }
        }
        return new Product();
    }

    public List<Order> populateOrders(){
        List<Order> orders = new ArrayList<Order>();
        return new ArrayList<Order>();
    }
}

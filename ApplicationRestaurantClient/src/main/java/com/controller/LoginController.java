package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Alin on 5/21/2016.
 */

@Controller
public class LoginController {

    @RequestMapping("/403")
    public String accessDeniedPage(){
        return "403";
    }
}
